﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LeagueApp
{
    public partial class moreInfoPage : ContentPage
    {
        public moreInfoPage(string detailedCombat)
        {
            string holder = detailedCombat;
            InitializeComponent();
            detailedText.Text = holder;
        }
    }
}
