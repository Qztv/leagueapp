﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System;
namespace LeagueApp.Champions
{
    public class Vayne
    {
        public double initial_ad;// = 92.2;
        public double initial_as;// = .625 / .96;
        public  int crit_chance_marker = 60;
        public  int silver_bolts_marker = 0;
        public  int crit_chance = 0;

        //double atkspeed = .625 / .96;
        double ad;// = 92.2;

        public string emptyHold = "";

        public Vayne()
        {
            getStats();

        }

        public async void getStats()
        {
            AWSConfigs.AWSRegion = "us-west-2";
            CognitoAWSCredentials credentials = new CognitoAWSCredentials("us-west-2:76f2cbb7-5aca-4821-9488-d026ef3c3374", RegionEndpoint.USWest2);
            var client = new AmazonDynamoDBClient(credentials, RegionEndpoint.USWest2);
            DynamoDBContext context = new DynamoDBContext(client);
            champion champ = await context.LoadAsync<champion>(1);
            initial_ad = champ.AD;
            initial_as = champ.AS * (1 + champ.BonusAS);
            ad = initial_ad;


        }

        public int crit_maker()
        {
            Random random = new Random();

            int xRan = random.Next(1, 100);



            return xRan;
        }

        /*
        void Vayne::Marker(Vayne &attacker, int num)
        {
            attacker.silver_bolts_marker= num;
            return;

        }
        int Vayne::getMarker(Vayne attacker)
        {

            return attacker.silver_bolts_marker;

        }*/

        public double Attack(ref Vayne attacker, Darius defender,ref string attckHold)
        {

            int xRan = crit_maker();

            double silver_bolts_output;



            double damage = attacker.ad;  //setting base attack damage
                                          //damage=item.getItems(damage);
            double damage_dealt;

            //silver bolts stuff


            //cout << attacker.crit_chance << endl;
            if (xRan < attacker.crit_chance) //if its a crit
            {
                attacker.silver_bolts_marker++;



                damage_dealt = (damage * 2) * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;

                    attckHold += "you did true damage and crit: \n" + damage_dealt;
                    attckHold += "HP left: " + defender.hp + "\n\n";

                    return damage_dealt;

                }
                else
                    //cout << "DMG (CRITICAL) : " << damage_dealt << "\n\n\n";

                return damage_dealt;
            }
            else  //its not a crit
            {
                attacker.silver_bolts_marker++;

                //cout << attacker.silver_bolts_marker << endl;

                damage_dealt = damage * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    double trueDealt = 0;
                    double basicDmg = damage_dealt;
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;

                    trueDealt = defender.max_health * .12;

                    silver_bolts_output = defender.max_health * .12;

                    attckHold += "SILVERBOLTS PROC! ( " + trueDealt + " ) \n";

                    attckHold += "Basic attack did " + basicDmg + "\n\n";

                    //attckHold += "HP left: " + defender.hp + "\n\n";

                    return damage_dealt;

                    //cout << "true damage" << "( " << silver_bolts_output << " )" << endl;
                    //cout << "DMG: " << damage_dealt << "\n\n\n";
                }
                else
                {   attckHold += "Basic Attack: " + damage_dealt + "\n\n";
                    
                }
                    

                return damage_dealt;
            }
        }


        public double Attack2(ref Vayne attacker, Rek defender,ref string attckHold)
        {

            int xRan = crit_maker();

            double silver_bolts_output;



            double damage = attacker.ad;  //setting base attack damage
                                          //damage=item.getItems(damage);
            double damage_dealt;

            //silver bolts stuff


            //cout << attacker.crit_chance << endl;
            if (xRan < attacker.crit_chance) //if its a crit
            {
                attacker.silver_bolts_marker++;

                //cout << attacker.silver_bolts_marker << endl;

                damage_dealt = (damage * 2) * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;

                    attckHold += "you did true damage and crit: \n" + damage_dealt;


                    return damage_dealt;
                }
                else
                {
                    //cout << "DMG (CRITICAL) : " << damage_dealt << "\n\n\n";
                    return damage_dealt;
                }
                    //cout << "DMG (CRITICAL) : " << damage_dealt << "\n\n\n";


            }
            else  //its not a crit
            {
                attacker.silver_bolts_marker++;

                //cout << attacker.silver_bolts_marker << endl;

                damage_dealt = damage * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    double trueDealt = 0;
                    double basicDmg = damage_dealt;
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;
                    trueDealt += defender.max_health * .12;

                    silver_bolts_output = defender.max_health * .12;

                    attckHold += "SILVERBOLTS PROC! ( " + trueDealt + " ) \n";

                    attckHold += "Basic attack did " + basicDmg + "\n\n";

                    //cout << "true damage" << "( " << silver_bolts_output << " )" << endl;
                    //cout << "DMG: " << damage_dealt << "\n\n\n";
                    return damage_dealt;
                }
                else
                {
                    attckHold += "Basic Attack: " + damage_dealt + "\n\n";

                }

                return damage_dealt;

            }

        }

        public double Attack3(ref Vayne attacker, ChoGath defender,ref string attckHold)
        {

            int xRan = crit_maker();

            double silver_bolts_output;



            double damage = attacker.ad;  //setting base attack damage
                                          //damage=item.getItems(damage);
            double damage_dealt;

            //silver bolts stuff


            //cout << attacker.crit_chance << endl;
            if (xRan < attacker.crit_chance) //if its a crit
            {
                attacker.silver_bolts_marker++;



                damage_dealt = (damage * 2) * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;
                    attckHold += "you did true damage and crit: \n" + damage_dealt;
                    return damage_dealt;
                }
                else
                {
                    //cout << "DMG (CRITICAL) : " << damage_dealt << "\n\n\n";
                    return damage_dealt;
                }
                //cout << "DMG (CRITICAL) : " << damage_dealt << "\n\n\n";


            }
            else  //its not a crit
            {
                attacker.silver_bolts_marker++;

                //cout << attacker.silver_bolts_marker << endl;

                damage_dealt = damage * defender.armor_Reduction();
                if (attacker.silver_bolts_marker == 3)
                {
                    double trueDealt = 0;
                    double basicDmg = damage_dealt;
                    attacker.silver_bolts_marker = 0;
                    damage_dealt += defender.max_health * .12;
                    trueDealt += defender.max_health * .12;

                    silver_bolts_output = defender.max_health * .12;

                    attckHold += "SILVERBOLTS PROC! ( " + trueDealt + " ) \n";

                    attckHold += "Basic attack did " + basicDmg + "\n\n";


                    return damage_dealt;
                }
                else
                    attckHold += "Basic Attack: " + damage_dealt + "\n\n";

                
                return damage_dealt;
            }

        }

        public int Calculate(Vayne attacker, Darius defender,ref string holder)
        {
            
            //set the attack count
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";
            //if defender is living keep attacking
            while (defender.hp > 0)
            {

                //cout << "HP: " << defender.hp << endl;

                //up the count, as we can hit living champion
                attack_count++;

                //the new hp is done after the dmg done
                defender.hp -= attacker.Attack(ref attacker, defender,ref holder);

                holder += "HP REMAINING: " + defender.hp + "\n";

                emptyHold = holder;

            } //end of while loop

            defender.hp = defender.max_health; //setting health back to ful after calculations
            return attack_count;

           
        }




        public int Calculate2(Vayne attacker, Rek defender,ref string holder)
        {
            //set the attack count
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";

            //if defender is living keep attacking
            while (defender.hp > 0)
            {

                //cout << "HP: " << defender.hp << endl;

                //up the count, as we can hit living champion
                attack_count++;

                //the new hp is done after the dmg done
                defender.hp -= attacker.Attack2(ref attacker, defender,ref holder);

                holder += "HP REMAINING: " + defender.hp + "\n";


            } //end of while loop
            defender.hp = defender.max_health; //setting health back to ful after calculations
            return attack_count;

            //final output as the loop condition shows all hits but last in output 
            //cout << "HP: " << defender.hp << "    DEAD!" << endl;

            //cout << "it took you " << attack_count << " attacks , GG.";
        }

        public int Calculate3(Vayne attacker, ChoGath defender,ref string holder)
        {
            //set the attack count
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";

            //if defender is living keep attacking
            while (defender.hp > 0)
            {



                //up the count, as we can hit living champion
                attack_count++;

                //the new hp is done after the dmg done
                defender.hp -= attacker.Attack3(ref attacker, defender,ref holder);

                holder += "HP REMAINING: " + defender.hp + "\n";


            } //end of while loop
            defender.hp = defender.max_health; //setting health back to ful after calculations
            return attack_count;

            //final output as the loop condition shows all hits but last in output 
            //cout << "HP: " << defender.hp << "    DEAD!" << endl;

            //cout << "it took you " << attack_count << " attacks , GG.";
        }






    }

    internal interface IAmazonS3
    {
    }
}
