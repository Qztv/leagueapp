﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System;
namespace LeagueApp.Champions
{
    public class ChoGath
    {
        public double max_health;// = 2015;
        //2015
        public double hp;// = 2015;
        public double armor;// = 96.8;
        public double mr;// = 53.4;

        public ChoGath()
        {
            getStats();
        }

        public async void getStats()
        {
            AWSConfigs.AWSRegion = "us-west-2";
            CognitoAWSCredentials credentials = new CognitoAWSCredentials("us-west-2:76f2cbb7-5aca-4821-9488-d026ef3c3374", RegionEndpoint.USWest2);
            var client = new AmazonDynamoDBClient(credentials, RegionEndpoint.USWest2);
            DynamoDBContext context = new DynamoDBContext(client);
            champion champ = await context.LoadAsync<champion>(5);
            max_health = champ.HP;
            armor = champ.Armor;
            mr = champ.MR;
            hp = max_health;
        }

        public double get_effective()
        {
            double eHealth = (1 + (armor / 100)) * hp;

            return eHealth;
        }

        public double armor_Reduction()
        {
            if (armor >= 0)
            {
                return 100 / (100 + armor);
            }
            else
                return 2 - (100 / (100 - armor));
        }

        public double magic_Reduction()
        {
            if (mr >= 0)
            {
                return 100 / (100 + mr);
            }
            else
                return 2 - (100 / (100 - mr));
        }


    }
}
