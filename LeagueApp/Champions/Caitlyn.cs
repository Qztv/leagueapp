﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System;
namespace LeagueApp.Champions
{
    public class Caitlyn
    {
        
        double hs_modifier = 0;
        double crit_chance = 0;
        int headshot_marker = 0;
        double initial_as = 0;
        double ad = 99.1;
        //double atkspeed = 1.01101;

        public string emptyHold = "";

        public Caitlyn()
        {
            getStats();
        }

        public async void getStats()
        {
            AWSConfigs.AWSRegion = "us-west-2";
            CognitoAWSCredentials credentials = new CognitoAWSCredentials("us-west-2:76f2cbb7-5aca-4821-9488-d026ef3c3374", RegionEndpoint.USWest2);
            var client = new AmazonDynamoDBClient(credentials, RegionEndpoint.USWest2);
            DynamoDBContext context = new DynamoDBContext(client);
            champion champ = await context.LoadAsync<champion>(2);
            ad = champ.AD;
            initial_as = champ.AS * (1 + champ.BonusAS);
        }
                              
        public int Crit_maker()
        {
            Random random = new Random();
            int xRan = random.Next(1,100);

            return xRan;
        }

        double Headshot_percentage(Caitlyn attacker)
        {


            double crit_holder = attacker.crit_chance;   //make that into decimal form

            crit_holder /= 100;
            /* FOR WHEN IE BOOLEAN IS IMPLEMENTED
                if(attacker.isIEt == true)
                {
                    headshot_percent = (.50 + crit_chance + .25);

                    return headshot_percent;
                }
                else
                {
                    headshot_percent = (.50 + crit_chance);
                    return headshot_percent;
                }


            */



            hs_modifier = (.50 + crit_holder);



            return hs_modifier;

        }

        double Attack(ref Caitlyn attacker, Darius defender,ref string attckHold)
        {
            int xRan = Crit_maker();





            double damage = attacker.ad;  //setting base attack damage
                                          //damage=item.getItems(damage);
            double damage_dealt = 0;




            //cout << attacker.crit_chance << endl;
            if (xRan < attacker.crit_chance) //if its a crit
            {
                attacker.headshot_marker++;

                //cout << attacker.headshot_marker << endl;


                if (attacker.headshot_marker == 6)
                {
                    attacker.headshot_marker = 0;

                    damage_dealt = ((damage + damage * Headshot_percentage(attacker)) * 2) * defender.armor_Reduction();
                    attckHold += "you HEADSHOTTED  and crit: \n" + damage_dealt;
                    attckHold += "HP left: " + defender.hp + "\n\n";

                    return damage_dealt;
                }
                else
                {
                    damage_dealt = (damage * 2) * defender.armor_Reduction();
                    attckHold += "CRITICAL HIT: " + damage_dealt + "\n\n";

                }


                return damage_dealt;
            }
            else  //its not a crit
            {
                attacker.headshot_marker++;

                //cout << attacker.headshot_marker << endl;


                if (attacker.headshot_marker == 6)
                {
                    attacker.headshot_marker = 0;
                    damage_dealt = (damage * Headshot_percentage(attacker) + damage) * defender.armor_Reduction();

                    attckHold += "HEADSHOT! ( " + damage_dealt + " )\n";
                    attckHold += "HP left: " + defender.hp + "\n\n";

                   

                    return damage_dealt;



                    //cout << "headshot damage" << "( " << headshot_output << " )" << endl;
                    //cout << "DMG: " << damage_dealt << "\n\n\n";
                }
                else
                {
                    damage_dealt = damage * defender.armor_Reduction();
                    attckHold += "Basic attack did: " + damage_dealt + "\n\n";

                }
                return damage_dealt;
            }
        }

        double Attack2(ref Caitlyn attacker, Rek defender,ref string attckHold)
        {
            int xRan = Crit_maker();



            double headshot_modifier = Headshot_percentage(attacker);

            double damage = attacker.ad;  //setting base attack damage
                                          //damage=item.getItems(damage);
            double damage_dealt = 0;




            //cout << attacker.crit_chance << endl;
            if (xRan < attacker.crit_chance) //if its a crit
            {
                attacker.headshot_marker++;

                //cout << attacker.headshot_marker << endl;


                if (attacker.headshot_marker == 6)
                {
                    attacker.headshot_marker = 0;

                    damage_dealt = ((damage * Headshot_percentage(attacker)) + damage * 2) * defender.armor_Reduction();

                    attckHold += "you HEADSHOTTED and crit: \n" + damage_dealt;
                    attckHold += "HP left: " + defender.hp + "\n\n";

                    return damage_dealt;
                }
                else
                {
                    damage_dealt = (damage * 2) * defender.armor_Reduction();
                    attckHold += "CRITICAL HIT: " + damage_dealt + "\n\n";
                }


                return damage_dealt;
            }
            else  //its not a crit
            {
                attacker.headshot_marker++;

                //cout << attacker.headshot_marker << endl;


                if (attacker.headshot_marker == 6)
                {
                    attacker.headshot_marker = 0;
                    damage_dealt = (damage * Headshot_percentage(attacker) + damage) * defender.armor_Reduction();

                    attckHold += "HEADSHOT! ( " + damage_dealt + " )\n";
                    attckHold += "HP left: " + defender.hp + "\n\n";

                }
                else
                {
                    damage_dealt = damage * defender.armor_Reduction();
                    attckHold += "Basic attack did: " + damage_dealt + "\n\n";
                }


                return damage_dealt;
            }
        }

        double Attack3(ref Caitlyn attacker, ChoGath defender,ref string attckHold)
        {
            int xRan = Crit_maker();



            double headshot_modifier = Headshot_percentage(attacker);

            double damage = attacker.ad;  //setting base attack damage
                                          //damage=item.getItems(damage);
            double damage_dealt = 0;




            //cout << attacker.crit_chance << endl;
            if (xRan < attacker.crit_chance) //if its a crit
            {
                attacker.headshot_marker++;

                //cout << attacker.headshot_marker << endl;


                if (attacker.headshot_marker == 6)
                {
                    attacker.headshot_marker = 0;

                    damage_dealt = ((damage * Headshot_percentage(attacker)) + damage * 2) * defender.armor_Reduction();
                    attckHold += "you HEADSHOTTED and crit: \n" + damage_dealt;
                    attckHold += "HP left: " + defender.hp + "\n\n";

                    return damage_dealt;
                }
                else
                {
                    
                    damage_dealt = (damage * 2) * defender.armor_Reduction();
                    attckHold += "CRITICAL HIT: " + damage_dealt + "\n\n";
                }


                return damage_dealt;
            }
            else  //its not a crit
            {
                attacker.headshot_marker++;

                //cout << attacker.headshot_marker << endl;


                if (attacker.headshot_marker == 6)
                {
                    attacker.headshot_marker = 0;
                    damage_dealt = (damage * Headshot_percentage(attacker) + damage) * defender.armor_Reduction();

                    attckHold += "HEADSHOT! ( " + damage_dealt + " )\n";
                    attckHold += "HP left: " + defender.hp + "\n\n";

                }
                else
                {
                    damage_dealt = damage * defender.armor_Reduction();
                    attckHold += "Basic attack did: " + damage_dealt + "\n\n";
                }


                return damage_dealt;
            }
        }

        public int Calculate1(Caitlyn attacker, Darius defender,ref string holder)
        {
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";
            //if defender is living keep attacking
            while (defender.hp > 0)
            {

                //cout << "HP: " << defender.hp << endl;

                //up the count, as we can hit living champion
                attack_count++;

                //the new hp is done after the dmg done
                defender.hp -= attacker.Attack(ref attacker, defender,ref holder);

                holder += "HP REMAINING: " + defender.hp + "\n";


            } //end of while loop
            defender.hp = defender.max_health; //setting health back to ful after calculations
            return attack_count;

            //final output as the loop condition shows all hits but last in output 
            //cout << "HP: " << defender.hp << "    DEAD!" << endl;

            //cout << "it took you " << attack_count << " attacks , GG.";
        }

        public int Calculate2(Caitlyn attacker, Rek defender,ref string holder)
        {
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";

            //if defender is living keep attacking
            while (defender.hp > 0)
            {

                //cout << "HP: " << defender.hp << endl;

                //up the count, as we can hit living champion
                attack_count++;

                //the new hp is done after the dmg done
                defender.hp -= attacker.Attack2(ref attacker, defender,ref holder);

                holder += "HP REMAINING: " + defender.hp + "\n";

                emptyHold = holder;


            } //end of while loop
            defender.hp = defender.max_health; //setting health back to ful after calculations
            return attack_count;

            //final output as the loop condition shows all hits but last in output 
            //cout << "HP: " << defender.hp << "    DEAD!" << endl;

            //cout << "it took you " << attack_count << " attacks , GG.";
        }

        public int Calculate3(Caitlyn attacker, ChoGath defender, ref string holder)
        {
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";

            //if defender is living keep attacking
            while (defender.hp > 0)
            {

                //cout << "HP: " << defender.hp << endl;

                //up the count, as we can hit living champion
                attack_count++;

                //the new hp is done after the dmg done
                defender.hp -= attacker.Attack3(ref attacker, defender,ref holder);

                holder += "HP REMAINING: " + defender.hp + "\n";

                emptyHold = holder;


            } //end of while loop
            defender.hp = defender.max_health; //setting health back to ful after calculations
            return attack_count;

            //final output as the loop condition shows all hits but last in output 
            //cout << "HP: " << defender.hp << "    DEAD!" << endl;

            //cout << "it took you " << attack_count << " attacks , GG.";
        }




    }
}
