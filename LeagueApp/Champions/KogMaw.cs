﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System;
namespace LeagueApp.Champions
                   
{
    public class KogMaw
    {
        public double initial_ad;// = 106;
        public double initial_as;// = 1.30; //passive q attack speed

        public int crit_chance = 0;

        public double atkspeed;// = 1.30;
        public double ad;// = 92.2;
        public int W_CD = 8;
        public bool W;

        public string emptyHold = "";

        public KogMaw()
        {
            getStats();
        }

        public async void getStats()
        {
            AWSConfigs.AWSRegion = "us-west-2";
            CognitoAWSCredentials credentials = new CognitoAWSCredentials("us-west-2:76f2cbb7-5aca-4821-9488-d026ef3c3374", RegionEndpoint.USWest2);
            var client = new AmazonDynamoDBClient(credentials, RegionEndpoint.USWest2);
            DynamoDBContext context = new DynamoDBContext(client);
            champion champ = await context.LoadAsync<champion>(3);
            initial_ad = champ.AD;
            initial_as = champ.AS;
            ad = initial_ad;
            atkspeed = initial_as;
        }

        public int crit_maker()
        {
            Random random = new Random();

            int xRan = random.Next(1, 100);

            return xRan;
        }

        double Attack(ref KogMaw attacker, Darius defender,ref string attckHold)
        {
            int xRan = crit_maker();
            double damage = attacker.ad;  //setting base attack damag                                 //damage=item.getItems(damage);
            double damage_dealt = 0;

            //cout << attacker.crit_chance << endl;
            if (xRan < attacker.crit_chance) //if its a crit
            {
                damage_dealt = (damage * 2) * defender.armor_Reduction();

                attckHold += "CRITICAL HIT: " + damage_dealt + "\n\n";

                return damage_dealt;
            }
            else  //its not a crit
            {

                damage_dealt = damage * defender.armor_Reduction();

                attckHold += "Basic attack did " + damage_dealt + "\n";

                return damage_dealt;
            }
        }

        double Attack2(ref KogMaw attacker, Rek defender, ref string attckHold)
        {
            int xRan = crit_maker();
            double damage = attacker.ad;  //setting base attack damag                                 //damage=item.getItems(damage);
            double damage_dealt = 0;

            //cout << attacker.crit_chance << endl;
            if (xRan < attacker.crit_chance) //if its a crit
            {
                damage_dealt = (damage * 2) * defender.armor_Reduction();

                attckHold += "CRITICAL HIT: " + damage_dealt + "\n\n";

                return damage_dealt;
            }
            else  //its not a crit
            {

                damage_dealt = damage * defender.armor_Reduction();

                attckHold += "Basic attack did " + damage_dealt + "\n";

                return damage_dealt;
            }
        }
        double Attack3(ref KogMaw attacker, ChoGath defender,ref string attckHold)
        {
            int xRan = crit_maker();
            double damage = attacker.ad;  //setting base attack damag                                 //damage=item.getItems(damage);
            double damage_dealt = 0;

            //cout << attacker.crit_chance << endl;
            if (xRan < attacker.crit_chance) //if its a crit
            {
                damage_dealt = (damage * 2) * defender.armor_Reduction();

                attckHold += "CRITICAL HIT: " + damage_dealt + "\n\n";

                return damage_dealt;
            }
            else  //its not a crit
            {

                damage_dealt = damage * defender.armor_Reduction();

                attckHold += "Basic attack did " + damage_dealt + "\n";

                return damage_dealt;
            }
        }
        /*bool W_activate(int CD)
        {
            //double x=0;
            if (CD > 0)
                return true;
            else
                return false;
        }
        */

        public int Calculate(KogMaw attacker, Darius defender,ref string holder)
        {
            int attack_count = 0;
            double W_Damage = .06 * defender.max_health;

            holder += "STARTING HP: " + defender.hp + "\n";

            bool w = true;
            int numWAttacks = (int)Math.Floor((atkspeed * 8)); //rounds down
            int resetW = (int)Math.Floor((atkspeed * 17)) + 8; //rounds down 


            while(defender.hp > 0)
            {
                
                attack_count++;


                if((attack_count % numWAttacks != 0) && w == true )
                {
                    defender.hp -= attacker.Attack(ref attacker, defender, ref holder);
                    defender.hp -= W_Damage * defender.magic_Reduction();

                    holder += "W SHRED: " + W_Damage + "\n\n";

                    holder += "HP REMAINING: " + defender.hp + "\n";
                }
                else
                {
                    w = false;
                    if(attack_count % resetW == 0)
                    {
                        w = true;
                    }
                    else
                    {
                        holder += "\n HP REMAINING: " + defender.hp + "(no W) \n";
                        defender.hp -= attacker.Attack(ref attacker, defender, ref holder);//do nothing if not ready to reset

                        //holder += "HP REMAINING: " + defender.hp + "\n";
                        holder += "\n";
                    }
                }
            }
            defender.hp = defender.max_health;
            return attack_count;

        }






        public int Calculate2(KogMaw attacker, Rek defender,ref string holder)
        {
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";

            bool w = true;
            int numWAttacks = (int)Math.Floor((atkspeed * 8)); //rounds down
            int resetW = (int)Math.Floor((atkspeed * 17)) + 8; //rounds down 


            while (defender.hp > 0)
            {

                attack_count++;


                if ((attack_count % numWAttacks != 0) && w == true)
                {
                    defender.hp -= attacker.Attack2(ref attacker, defender, ref holder); //regular attack still goes
                    double W_Damage = .06 * defender.max_health;
                    defender.hp -= W_Damage * defender.magic_Reduction();

                    holder += "W SHRED: " + W_Damage + "\n\n";

                    holder += "HP REMAINING: " + defender.hp + "\n";

                }
                else
                {
                    w = false;
                    if (attack_count % resetW == 0)
                    {
                        w = true;
                    }
                    else
                    {
                        holder += "\n HP REMAINING: " + defender.hp + " (no W) \n";
                        defender.hp -= attacker.Attack2(ref attacker, defender, ref holder);//do nothing if not ready to reset

                        //holder += "HP REMAINING: " + defender.hp + "\n";
                        holder += "\n";
                    }
                }
            }
            defender.hp = defender.max_health;
            return attack_count;
        }



        public Int32 Calculate3(KogMaw attacker, ChoGath defender,ref string holder)
        {
            int attack_count = 0;

            holder += "STARTING HP: " + defender.hp + "\n";

            bool w = true;
            int numWAttacks = (int)Math.Floor((atkspeed * 8)); //rounds down
            int resetW = (int)Math.Floor((atkspeed * 17)) + 8; //rounds down 


            while (defender.hp > 0)
            {

                attack_count++;


                if ((attack_count % numWAttacks != 0) && w == true)
                {
                    defender.hp -= attacker.Attack3(ref attacker, defender, ref holder); //regular attack still goes
                    double W_Damage = .06 * defender.max_health;
                    defender.hp -= W_Damage * defender.magic_Reduction();

                    holder += "W SHRED: " + W_Damage + "\n\n";

                    holder += "HP REMAINING: " + defender.hp + "\n";

                }
                else
                {
                    w = false;
                    if (attack_count % resetW == 0)
                    {
                        w = true;
                    }
                    else
                    {
                        holder += "\n HP REMAINING: " + defender.hp + " (no W) \n";

                        defender.hp -= attacker.Attack3(ref attacker, defender, ref holder);//do nothing if not ready to reset
                       
                        //holder += "HP REMAINING: " + defender.hp + "\n";
                        holder += "\n";
                    }
                }
            }
            defender.hp = defender.max_health;
            return attack_count;
        }
    }
}
