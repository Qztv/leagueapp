﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeagueApp.Champions
{
    [DynamoDBTable("Champion")]
    class champion
    {
        [DynamoDBHashKey]
        public int ID { get; set; }
        public string name { get; set; }
        public double AD { get; set; }
        public double AS { get; set; }
        public double BonusAS { get; set; }
        public double Armor { get; set; }
        public double HP { get; set; }
        public double MR { get; set; }
        public double Mana { get; set; }

    }
}
