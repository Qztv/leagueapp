﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LeagueApp
{
    public partial class startingPage : ContentPage
    {
        
    
        public startingPage()
        {
            NavigationPage.SetHasNavigationBar(this,false);
            InitializeComponent();
        }


        void Main_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new LeagueAppPage());
        }
    }
}
