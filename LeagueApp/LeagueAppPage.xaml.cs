﻿using Xamarin.Forms;
using LeagueApp.Champions;




namespace LeagueApp
{
    public partial class LeagueAppPage : ContentPage
    {
        int attacker = 0; //to see which attacker object to make
        int defender = 0; //to see which defender object to make

        Vayne vayne = new Vayne();
        Caitlyn caitlyn = new Caitlyn();
        Darius darius = new Darius();
        Rek rek = new Rek();
        ChoGath cho = new ChoGath();
        KogMaw kog = new KogMaw();

        string empty = "";

        
        public LeagueAppPage()
        {
            
            InitializeComponent();
        }

        void Vayne_Clicked(object sender, System.EventArgs e)
        {
            
            attacker = 1;
            caitlynButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            kogButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            vayneButton.BackgroundColor = Xamarin.Forms.Color.White;


            adc_Selected.TextColor = Xamarin.Forms.Color.Green;
            adc_Selected.Text = "Vayne Selected";
        }

        void Caitlyn_Clicked(object sender, System.EventArgs e)
        {
            attacker = 2;
            vayneButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            kogButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            caitlynButton.BackgroundColor = Xamarin.Forms.Color.White;
            adc_Selected.TextColor = Xamarin.Forms.Color.Green;
            adc_Selected.Text = "Caitlyn Selected";
        }

        void Kog_Clicked(object sender, System.EventArgs e)
        {
            attacker = 3;
            vayneButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            caitlynButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            kogButton.BackgroundColor = Xamarin.Forms.Color.White;
            adc_Selected.TextColor = Xamarin.Forms.Color.Green;
            adc_Selected.Text = "Kog'Maw Selected";
        }

        void Darius_Clicked(object sender, System.EventArgs e)
        {
            defender = 1;
            rekButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            choButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            dariusButton.BackgroundColor = Xamarin.Forms.Color.White;
            tank_Selected.TextColor = Xamarin.Forms.Color.Firebrick;
            tank_Selected.Text = "Darius Selected";
        }

        void Reksai_Clicked(object sender, System.EventArgs e)
        {
            defender = 2;
            dariusButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            choButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            rekButton.BackgroundColor = Xamarin.Forms.Color.White;
            tank_Selected.TextColor = Xamarin.Forms.Color.Firebrick;
            tank_Selected.Text = "Rek'sai Selected";
        }

        void Cho_Clicked(object sender, System.EventArgs e)
        {
            defender = 3;
            dariusButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            rekButton.BackgroundColor = Xamarin.Forms.Color.Transparent;
            choButton.BackgroundColor = Xamarin.Forms.Color.White;
            tank_Selected.TextColor = Xamarin.Forms.Color.Firebrick;
            tank_Selected.Text = "Chogath Selected";
        }


        void Calculate_Clicked(object sender, System.EventArgs e)
        {
            
            detailsButton.Text = "more info";
            if(attacker == 0)
            {
                adc_Selected.TextColor = Xamarin.Forms.Color.Red;
                adc_Selected.Text = "You have not selected an ADC!";

            }
            if(defender == 0)
            {
                tank_Selected.TextColor = Xamarin.Forms.Color.Red;
                tank_Selected.Text = "you have not selected a TANK!";
            }


            //didnt do it in attackers class / defender class so this part is just alot of ifs, in retrospect would have done it the other way (time constraint)
            //vayne darius 
            if(attacker ==1 && defender ==1)
            {
                empty = "";
                Calculate.IsEnabled = false;

                double referenceOfHealth = darius.hp;
                Label tank = new Label();
                tank.Text = "Darius";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Vayne";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks =vayne.Calculate(vayne,darius,ref empty);

                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                if(darius.hp < 0)
                {
                    darius.hp = referenceOfHealth; 
                }
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });


                calculateOutput.FormattedText = formated;


                Calculate.IsEnabled = true;

            }
            //vayne reksai
            if(attacker == 1 && defender == 2)
            {
                empty = "";
                Calculate.IsEnabled = false;
                Calculate.Text = "Calculating ...";
                double referenceOfHealth = rek.hp;
                Label tank = new Label();
                tank.Text = "Reksai";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Vayne";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = vayne.Calculate2(vayne, rek,ref empty);
                if(rek.hp < 0)
                {
                    rek.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                }
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });


                calculateOutput.FormattedText = formated;
                Calculate.IsEnabled = true;
            }

            //vayne cho
            if (attacker == 1 && defender == 3)
            {
                empty = "";
                Calculate.IsEnabled = false;
                Calculate.Text = "Calculating ...";
                double referenceOfHealth = cho.hp;
                Label tank = new Label();
                tank.Text = "Cho'Gath";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Vayne";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = vayne.Calculate3(vayne, cho, ref empty);
                if (cho.hp < 0)
                {
                    cho.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                }
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });


                calculateOutput.FormattedText = formated;
                Calculate.IsEnabled = true;
            }

            //caitlyn darius
            if (attacker == 2 && defender == 1)
            {
                empty = "";
                Calculate.IsEnabled = false;
                Calculate.Text = "Calculating ...";
                double referenceOfHealth = darius.hp;
                Label tank = new Label();
                tank.Text = "Darius";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Caitlyn";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = 0;
                if (darius.hp < 0)
                {
                    darius.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                    numAttacks = caitlyn.Calculate1(caitlyn, darius,ref empty);
                }
                else
                    numAttacks = caitlyn.Calculate1(caitlyn, darius,ref empty);


              

                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });


                calculateOutput.FormattedText = formated;
                Calculate.IsEnabled = true;
            }

            //caitlyn rek
            if (attacker == 2 && defender == 2)
            {
                empty = "";
                Calculate.IsEnabled = false;
                Calculate.Text = "Calculating ...";
                double referenceOfHealth = rek.max_health;
                Label tank = new Label();
                tank.Text = "Reksai";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Caitlyn";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = 0;
                if (rek.hp < 0)
                {
                    rek.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                    numAttacks = caitlyn.Calculate2(caitlyn, rek,ref empty);
                }
                else
                    numAttacks = caitlyn.Calculate2(caitlyn, rek,ref empty);
                
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick});


                calculateOutput.FormattedText = formated;
                Calculate.IsEnabled = true;
            }


            //caitlyn cho
            if (attacker == 2 && defender == 3)
            {
                empty = "";
                Calculate.IsEnabled = false;
                Calculate.Text = "Calculating ...";
                double startHp = cho.hp;
                Label tank = new Label();
                tank.Text = "Cho'Gath";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Caitlyn";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = 0;
                if (cho.hp < 0)
                {
                    cho.hp = startHp; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                    numAttacks = caitlyn.Calculate3(caitlyn, cho,ref empty);
                }
                else
                    numAttacks = caitlyn.Calculate3(caitlyn, cho,ref empty);
                
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });


                calculateOutput.FormattedText = formated;
                Calculate.IsEnabled = true;
            }

            ////kog darius
            if (attacker == 3 && defender == 1)
            {
                empty = "";
                Calculate.IsEnabled = false;
                Calculate.Text = "Calculating ...";
                double referenceOfHealth = darius.hp;
                Label tank = new Label();
                tank.Text = "Darius";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Kog'Maw";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = kog.Calculate(kog, darius,ref empty);
                if (darius.hp < 0)
                {
                    darius.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                }
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });


                calculateOutput.FormattedText = formated;
                Calculate.IsEnabled = true;
            }

            //kog reksai
            if(attacker ==3 && defender == 2)
            {
                empty = "";
                Calculate.IsEnabled = false;
                Calculate.Text = "Calculating ...";
                double referenceOfHealth = rek.hp;
                Label tank = new Label();
                tank.Text = "Reksai";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Kog'Maw";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = kog.Calculate2(kog, rek,ref empty);
                if (rek.hp < 0)
                {
                    rek.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                }
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });


                calculateOutput.FormattedText = formated;
                Calculate.IsEnabled = true;

            }

            //kog cho
            if(attacker ==3 && defender == 3)
            {
                empty = "";
                Calculate.IsEnabled = false;
                Calculate.Text = "Calculating ...";
                double referenceOfHealth = cho.hp;
                Label tank = new Label();
                tank.Text = "Cho'Gath";
                tank.TextColor = Xamarin.Forms.Color.Red;

                Label adc = new Label();
                adc.Text = "Kog'Maw";
                tank.TextColor = Xamarin.Forms.Color.Green;

                int numAttacks = kog.Calculate3(kog, cho,ref empty);
                if (cho.hp < 0)
                {
                    cho.hp = referenceOfHealth; // IMPORTANT during calculation hp attribute is reduced. So when doing items update the hp, this line fixes the issue of attacking when the defender is at 0, or will print 0 as "num attacks"
                }
                Label stringOfNumAttacks = new Label();
                stringOfNumAttacks.Text = numAttacks.ToString();
                stringOfNumAttacks.TextColor = Xamarin.Forms.Color.Gold;

                var formated = new FormattedString();
                formated.Spans.Add(new Span { Text = "It took " });
                formated.Spans.Add(new Span { Text = adc.Text, ForegroundColor = Xamarin.Forms.Color.Green });
                formated.Spans.Add(new Span { Text = " " });
                formated.Spans.Add(new Span { Text = stringOfNumAttacks.Text, ForegroundColor = Xamarin.Forms.Color.Gold });
                formated.Spans.Add(new Span { Text = " auto attacks to defeat " });
                formated.Spans.Add(new Span { Text = tank.Text, ForegroundColor = Xamarin.Forms.Color.Firebrick });


                calculateOutput.FormattedText = formated;
                Calculate.IsEnabled = true;
            }
            
        }










        void details_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new moreInfoPage(empty));
        }
    }
}
